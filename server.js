var express = require('express');
var app = express();
var jsf = require('json-schema-faker');


var data = require('./data.json');
var schemas = require('./schemas.json');


// ## CORS middleware
// see: http://stackoverflow.com/questions/7067966/how-to-allow-cors-in-express-nodejs
var allowCrossDomain = function(req, res, next) {

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.type('json');

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.sendStatus(200);
    }
    else {
      next();
    }
};
app.use(allowCrossDomain);

app.get('/:data/:page', function (req, res) {
  var page = req.params.page || 1;

    console.log('-------------', req.params);

  if (!req.params.data)
    res.status(500).send('[]');

  if (data[req.params.data] && data[req.params.data][page]) {
    return res.send(data[req.params.data][page]);
  }

  var result = [];

  for (var i = 0; i < 20; i++) {
    result.push(jsf(schemas[req.params.data]));
  }

  var response = {"data": result, "paginate": {"page": page, "total": 546}};

  if(!data[req.params.data])
      data[req.params.data] = {[page]: response};
  else
      data[req.params.data][page] = response;

  return res.send(data[req.params.data][page]);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});
